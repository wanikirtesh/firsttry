"use strict";

import { sleep } from "k6";

const users = JSON.parse(open('./users.json'));
export const options = { vus: 2, iterations:"10" };
export default function main() {
	var z = ((__ITER*options.vus)+__VU)%users.length;
	console.log(z);
}