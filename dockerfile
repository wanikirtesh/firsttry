FROM loadimpact/k6
COPY bin /G3_UI/bin
COPY data /G3_UI/data
COPY lib /G3_UI/lib
COPY test /G3_UI/test
WORKDIR /G3_UI/
EXPOSE 6565
ENTRYPOINT [ "sh", "/G3_UI/test/run.sh" ]
