"use strict";
import http from "k6/http";
import { Trend,Rate } from 'k6/metrics';

var client = require("../requestClient.js");

const host = "https://optix-backend.stage.ideasrms.com"
const host_login = "https://optix.stage.ideasrms.com"
const token = "Ly9Hdkh3NkNrREU5bHFtOG1ZQ1RJU2UxTHFyMVU2L3lERkNYaUFyc2lVMU1Edyswc3gwTG5ycjdUQXd4b2p6TjhEMFRXS24vY21zTlA4b29uU2wzNFc2cEJrZUJ0VXlYaVNHRkt0UDM2UEVqOWY5NEpldnViZmtzMlI0UDAwNCt2Uk13NGZ6WDJ1Rm9NOFluT2szcWpnTXEvK3VPcFhPTHAzWjJyL1E4MnBwcGE0bXF0eVpNcVpzakpYSVhlRTJj"
let restHeder = {
	"Referer":`${host_login}/`,
	"x-datadog-sampling-priority":`1`,
	"Accept-Language":`en-US,en;q=0.5`,
	"x-datadog-origin":`rum`,
	"Origin":`${host_login}`,
	"x-datadog-trace-id":`4006350136349490293`,
	"x-datadog-sampled":`1`,
	"Accept":`application/json, text/plain, */*`,
	"x-datadog-parent-id":`7582612721431717296`,
	"Accept-Encoding":`gzip, deflate, br`,
	"User-Agent":`Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0`
}

let optionsHeader = {
	"Referer":`${host_login}/`,
	"Accept-Language":`en-US,en;q=0.5`,
	"Origin":`${host_login}`,
	"Access-Control-Request-Headers":`adjustinventoryindicator,asofdate,authorization,comparisonasofdate,comparisondatetype,comparisonenddate,comparisonstartdate,content-type,currency,enddate,properties,startdate,useremail,x-datadog-origin,x-datadog-parent-id,x-datadog-sampled,x-datadog-sampling-priority,x-datadog-trace-id`,
	"Access-Control-Request-Method":`PUT`,
	"Accept-Encoding":`gzip, deflate, br`,
	"User-Agent":`Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0`,
	"Accept":`*/*`
}

let res;
let loginPageTrend = new Trend('login_Page');
let applyFilterTrend = new Trend('apply_Filter');
let dailyReviewReportTrend = new Trend('daily_review_Report');

export function login() {
	res = client.get(`/?g3Token=${token}`,host_login,
		{		
			"Accept-Language":"en-US,en;q=0.5",
			"Upgrade-Insecure-Requests":"1",
			"Accept-Encoding": "gzip, deflate, br",
			"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0",
			Accept:"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
		}
	);
	res = client.post(`/gemini/users/login`,host,{
			G3Token:`${token}`
		},
		{
			"Referer":`${host_login}/`,
			"x-datadog-sampling-priority":`1`,
			"Accept-Language":`en-US,en;q=0.5`,
			"x-datadog-origin":`rum`,
			"Origin":`${host_login}`,
			"x-datadog-trace-id":`6581104655725178947`,
			"x-datadog-sampled":`1`,
			"Accept":`application/json, text/plain, */*`,
			"x-datadog-parent-id":`3630282213194551853`,
			"Accept-Encoding":`gzip, deflate, br`,
			"User-Agent":`Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:87.0) Gecko/20100101 Firefox/87.0`
		},res.execTime
	);
	restHeder.authorization = `Bearer ${res.response.json("jwtToken")}`
	res = client.get(`/gemini/users/identity`,host,restHeder,res.execTime);
	res = client.get(`/gemini/filter/currency?clientCode=STAG`,host,restHeder,res.execTime);
	res = client.get(`/gemini/filter/properties?clientCode=STAG`,host,restHeder,res.execTime);
	res = client.get(`/gemini/actuator/info?clientCode=STAG`,host,restHeder,res.execTime);
	res = client.get(`/gemini/configuration/feature?clientCode=STAG`,host,restHeder,res.execTime);
	res = client.get(`/gemini/userPreferences?clientCode=STAG`,host,restHeder,res.execTime);
	res = client.get(`/gemini/filter/contexts?clientCode=STAG`,host,restHeder,res.execTime)
	res = client.get(`/gemini/propertyList?clientCode=STAG`,host,restHeder,res.execTime);
	loginPageTrend.add(res.execTime);
	
}

export function applyFilter(){
	res = client.options(`/gemini/userPreferences/updateDefaultFilterView/7?clientCode=STAG`,host,{},optionsHeader);
	
	let filterHeader = restHeder;
	filterHeader.asOfDate="2020-09-28";
	filterHeader.startDate="2020-01-01";
	filterHeader.adjustInventoryIndicator="false";
	filterHeader.currency="EUR";
	filterHeader["Content-Type"]="application/json";
	filterHeader.comparisonAsOfDate="2019-09-30";
	filterHeader.comparisonDateType="STLY";
	filterHeader.endDate="2020-12-31";
	filterHeader.comparisonStartDate="2019-01-02";
	filterHeader.comparisonEndDate="2020-01-02";
	filterHeader.properties="10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59";
	filterHeader.userEmail="internalUser@ideas.com";
	res = client.put(`/gemini/userPreferences/updateDefaultFilterView/7?clientCode=STAG`,host,{},filterHeader,res.execTime);
	filterHeader = restHeder;
	filterHeader.asOfDate=`2020-09-29`;
	filterHeader.startDate=`2020-09-15`;
	filterHeader.adjustInventoryIndicator=`false`;
	filterHeader.currency=`EUR`;
	filterHeader.comparisonAsOfDate=`2019-10-01`;
	filterHeader.comparisonDateType=`STLY`;
	filterHeader.endDate=`2020-10-15`;
	filterHeader.comparisonStartDate=`2019-09-17`;
	filterHeader.comparisonEndDate=`2019-10-17`;
	filterHeader.properties=`10,11,12,13,14`;
	filterHeader.userEmail=`internalUser@ideas.com`;
	res = client.get(`/gemini/filter/validateAsOfDate?clientCode=STAG`,host,filterHeader,res.execTime);
	applyFilterTrend.add(res.execTime);
}

export function dailyReviewReport(){
	optionsHeader['Access-Control-Request-Method']='GET'
	res = client.options(`/gemini/analysis/fm-template/4?clientCode=STAG`,host,	{},optionsHeader);
	let filterHeader = restHeder;
	filterHeader.asOfDate=`2020-09-29`;
	filterHeader.startDate=`2020-09-15`;
	filterHeader.adjustInventoryIndicator=`false`;
	filterHeader.currency=`EUR`;
	filterHeader.comparisonAsOfDate=`2019-10-01`;
	filterHeader.comparisonDateType=`STLY`;
	filterHeader.endDate=`2020-10-15`;
	filterHeader.comparisonStartDate=`2019-09-17`;
	filterHeader.comparisonEndDate=`2019-10-17`;
	filterHeader.properties=`10,11,12,13,14`;
	filterHeader.userEmail=`internalUser@ideas.com`;
	res = client.get('/gemini/analysis/fm-template/4?clientCode=STAG',host,filterHeader,res.execTime);
	
	optionsHeader['Access-Control-Request-Method']='GET'
	res = client.options('/gemini/analysis/async/pivot?clientCode=STAG',host,{},optionsHeader,res.execTime);
	res = client.get('/gemini/analysis/async/pivot?clientCode=STAG',host,filterHeader,res.execTime);
	let index = res.response.body;
	
	res = client.options(`/gemini/analysis/async/ticket/${index}?clientCode=STAG`,host,{},optionsHeader,res.execTime);
	res = client.get(`/gemini/analysis/async/ticket/${index}?clientCode=STAG`,host,restHeder,res.execTime);
	
	optionsHeader['Access-Control-Request-Method']='POST'
	res = client.options(`/gemini/fm-analysis/handshake`,host,{},optionsHeader,res.execTime);
	filterHeader.clientCode = 'STAG'
	res = client.post(`/gemini/fm-analysis/handshake`,host,'{"type":"handshake","version":"2.8.29"}',filterHeader,res.execTime);

	res = client.options(`/gemini/fm-analysis/fields`,host,{},optionsHeader,res.execTime);
	res = client.post(`/gemini/fm-analysis/fields`,host,`{"index":"${index}","type":"fields"}`,filterHeader,res.execTime);

	res = client.options(`/gemini/fm-analysis/members`,host,{},optionsHeader,res.execTime);
	res = client.post(`/gemini/fm-analysis/members`,host,
		`{"index":"${index}","type":"members","field":{"uniqueName":"propertyName"},"page":0}`,
		filterHeader,res.execTime);
	res = client.post(`/gemini/fm-analysis/members`,host,
		`{"index":"${index}","type":"members","field":{"uniqueName":"activityDate.Year"},"page":0}`,
		filterHeader,res.execTime);
	res = client.post(`/gemini/fm-analysis/members`,host,
		`{"index":"${index}","type":"members","field":{"uniqueName":"activityDate.Month"},"page":0}`,
		filterHeader,res.execTime);
	res = client.post(`/gemini/fm-analysis/members`,host,
		`{"index":"${index}","type":"members","field":{"uniqueName":"activityDate.Day"},"page":0}`,
		filterHeader,res.execTime);
	
	res = client.options(`/gemini/fm-analysis/select`,host,{},optionsHeader,res.execTime);
	res = client.post(`/gemini/fm-analysis/select`,host,
		`{"type":"select","index":"${index}","query":{"aggs":{"by":{"rows":[{"uniqueName":"propertyName"}]},"values":[{"func":"sum","field":{"uniqueName":"stlyActualRoomsNbr"}},{"func":"sum","field":{"uniqueName":"stlyActualRoomRevenueAmt"}},{"func":"sum","field":{"uniqueName":"roomsNbr"}},{"func":"sum","field":{"uniqueName":"roomRevenueAmt"}},{"func":"sum","field":{"uniqueName":"stlyRoomsNbr"}},{"func":"sum","field":{"uniqueName":"pd_rooms_nr"}},{"func":"sum","field":{"uniqueName":"pd_room_revenue_amt"}},{"func":"sum","field":{"uniqueName":"sevenday_rooms_nr"}},{"func":"sum","field":{"uniqueName":"sevenday_room_revenue_amt"}},{"func":"sum","field":{"uniqueName":"thirtyday_rooms_nr"}},{"func":"sum","field":{"uniqueName":"thirtyday_room_revenue_amt"}},{"func":"sum","field":{"uniqueName":"stlyRoomRevenueAmt"}},{"func":"sum","field":{"uniqueName":"rmgmtFcstUnitNbr"}},{"func":"sum","field":{"uniqueName":"rmgmtFcstRevenueAmt"}},{"func":"sum","field":{"uniqueName":"budgetUnitNbr"}},{"func":"sum","field":{"uniqueName":"budgetRevenueAmt"}}]}},"page":0}`,
		filterHeader,res.execTime);
	dailyReviewReportTrend.add(res.execTime);
}