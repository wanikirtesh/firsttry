"use strict";
import http from "k6/http";
var k6_1 = require("k6");
var printResponse = false;

export function get(path,baseURL,headers,execTime=0){
	let res = http.get(
		`${baseURL}${path}`,
		{
			headers:headers
		}
	);
	k6_1.check(res, { 'request succeeded': function (r) { return r.status < 400; } })
	if (res.status >= 400)
        k6_1.fail("GET " + path + " request failed with status " + res.status + ".\n" + res.body);
	execTime += res.timings.duration;
	if(printResponse)
		console.log(res.body);
	return {response:res,execTime:execTime};
}

export function post(path,baseURL,body,headers,execTime=0){
	let res = http.post(
		`${baseURL}${path}`,
		body,
		{
			headers:headers
		}
	);
	k6_1.check(res, { 'request succeeded': function (r) { return r.status < 400; } })
	if (res.status >= 400)
        k6_1.fail("POST " + path + " request failed with status " + res.status + ".\n" + res.body);
	execTime += res.timings.duration;
	if(printResponse)	
		console.log(res.body);
	return {response:res,execTime:execTime};
}

export function put(path,baseURL,body,headers,execTime=0){
	let res = http.put(
		`${baseURL}${path}`,
		body,
		{
			headers:headers
		}
	);
	k6_1.check(res, { 'request succeeded': function (r) { return r.status < 400; } })
	if (res.status >= 400)
        k6_1.fail("PUT " + path + " request failed with status " + res.status + ".\n" + res.body);
	execTime += res.timings.duration;
	if(printResponse)
		console.log(res.body);
	return {response:res,execTime:execTime};
}

export function options(path,baseURL,body,headers,execTime=0){
	let res = http.options(
		`${baseURL}${path}`,
		body,
		{
			headers:headers
		}
	);
	k6_1.check(res, { 'request succeeded': function (r) { return r.status < 400; } })
	if (res.status >= 400)
        k6_1.fail("OPTIONS " + path + " request failed with status " + res.status + ".\n" + res.body);
	execTime += res.timings.duration;
	if(printResponse)
		console.log(res.body);
	return {response:res,execTime:execTime};
}