"use strict";
import { jUnit, textSummary } from 'https://jslib.k6.io/k6-summary/0.0.1/index.js';

var optixPages = require("./bin/Optix/OptixPages.js");
//const propertyData = papaparse.parse(open('./properties.csv'), { header: true }).data;
export const options = { vus: 1, iterations:1 };

export default function main() {
	//let counter = ((__ITER*options.vus)+__VU)%propertyData.length;
	optixPages.login();
	optixPages.applyFilter();
	optixPages.dailyReviewReport()
}


export function handleSummary(data) {
    console.log('Preparing the end-of-test summary...');
    // Send the results to some remote server or trigger a hook
    return {
        'stdout': textSummary(data, { indent: ' ', enableColors: true}), // Show the text summary to stdout...
        './test-results/junit.xml': jUnit(data), // but also transform it and save it as a JUnit XML...
        './artifacts/summary.json': JSON.stringify(data), // and a JSON with all the details...
        // And any other JS transformation of the data you can think of,
        // you can write your own JS helpers to transform the summary data however you like!
    }
}