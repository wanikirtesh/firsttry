#!/bin/bash
set +e # turn off error-trapping
start=$((`date +%s`-30))
k6 run ./Optix_UI_Perf_Test.js
end=$((`date +%s`+30))

echo "DataDog Dashboard for the test"
echo "https://ideas.datadoghq.com/dashboard/ans-s54-cej/gemini-stage?from_ts=${start}000&live=false&to_ts=${end}000"